# Insta Cam #
Instagram knock-off example app for Android.

### Topics addressed: ###

* Implicit Intents
* Intent Filters
* Taking photos
* Using Cards
* Using tabs
* Rotating the Device
* Using Facebook for authentication