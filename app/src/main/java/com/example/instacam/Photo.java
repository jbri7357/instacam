package com.example.instacam;

import android.os.Environment;

import java.io.File;
import java.io.Serializable;
import java.util.UUID;

public class Photo implements Serializable {
    private static final File sDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
    private UUID mId;
    private String mCaption;
    private User mUser;

    Photo() {
        mId = UUID.randomUUID();
    }


    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public UUID getId() {
        return mId;
    }

    public File getFile() {
        return new File(sDirectory, mId.toString() + ".jpg");
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }


}
